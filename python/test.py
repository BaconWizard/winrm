import requests
import socket 
import winrm




# Connecting to windows machine.

try:
    session = winrm.Session('<ip>', auth=('<Account>','<password>'), transport="ntlm")
    result = session.run_ps("Get-EventLog -LogName System -Newest 5")
    print(result.std_out)

# Exception errors

except winrm.exceptions.InvalidCredentialsError:
    print("WinRm: cannot login")
except requests.exceptions.ConnectionError:
    print("refused connection: check perameters")
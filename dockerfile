FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y nano \
    python3-pip

RUN mkdir pywinrm

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY /python /pywinrm


